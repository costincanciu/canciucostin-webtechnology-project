import React, { Component } from 'react'

class LabelForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      labelName : ''
      
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render() {
    return (
      <div>
        Name : <input type="text" name="labelName" onChange={this.handleChange}/>
        <input type="button" className="Button" value="Add" onClick={() => this.props.onAdd({name : this.state.labelName})}/>
      </div>
    )
  }
}

export default LabelForm
