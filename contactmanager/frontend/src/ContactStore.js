import axios from 'axios'

const SERVER = 'https://seminar22-costincanciu.c9users.io'

class ContactStore{
  constructor(ee){
    this.ee = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/contacts')
      .then((response) => {
        this.content = response.data
        this.ee.emit('CONTACT_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(contact){
    axios.post(SERVER + '/contacts', contact)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/contacts/' + id) 
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, contact){
    axios.put(SERVER + '/contacts/' + id, contact)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  getOne(id){
    axios(SERVER + '/contacts/' + id)
      .then((response) => {
        this.selected = response.data
        this.ee.emit('SINGLE_CONTACT_LOAD')
      })
      .catch((error) => console.warn(error))
  }
}

export default ContactStore




