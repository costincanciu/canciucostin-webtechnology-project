import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import ContactList from './ContactList'
import Background from './b2.jpg'

const ee = new EventEmitter()


var sectionStyle = {
  width: "1580px",
  height: "850x",
  backgroundImage: "url(" + Background + ")"
};

class StartPage extends Component {
    
  constructor(props){
    super(props)
    this.state = {
      contacts : [],
      getStarted : -1,
      selected : null
    }

    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
  }
  componentDidMount(){

  }
  render() {
    if (this.state.getStarted === -1){
      return (
          <section style={ sectionStyle }>
          <div >
        <div>
               <h2>Contact Manager</h2>
                <div>  <input type="button" id="btnGetStarted" value="Get Started" onClick={() => this.setState({getStarted : true})}></input></div>
       
        </div>
          </div>
          </section>
      )
    }
    else{
      return (
        <div>
          <ContactList/>
        </div>  
      )
    }
  }
}

export default StartPage
