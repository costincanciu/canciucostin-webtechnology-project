import React, {Component} from 'react'

class Label extends Component{
  constructor(props){
    super(props)
    this.state = {
      isEditing : false,
      labelName : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  componentDidMount(){
    this.setState({
      labelName : this.props.label.name
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      isEditing : false
    })
  }
  render(){
    if (!this.state.isEditing){
      return (
        <div>
          <h5>{this.props.label.name}</h5>
          <div>
            <input type="button" className="Button" value="Delete" onClick={() => this.props.onDelete(this.props.label.id)} />
            <input type="button" className="Button" value="Edit" onClick={() => this.setState({isEditing : true})}/>
          </div>
        </div>  
      )
    }
    else{
      return (
        <div>
            <input type="text" name="labelName" onChange={this.handleChange} value={this.state.labelName}/>
            <input type="button" className="Button" value="Cancel" onClick={() => this.setState({isEditing : false})}/>
            <input type="button" className="Button" value="Save" onClick={() => this.props.onSave(this.props.label.id, {name : this.state.labelName})} />
        </div>
      )
    }
  }
}

export default Label