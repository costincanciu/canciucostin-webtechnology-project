import React, { Component } from 'react'
import Contact from './Contact'
import ContactForm from './ContactForm'
import ContactStore from './ContactStore'
import {EventEmitter} from 'fbemitter'
import ContactLabels from './ContactLabels'

const ee = new EventEmitter()
const store = new ContactStore(ee)

function addContact(contact){
  store.addOne(contact)
}

function deleteContact(id){
  store.deleteOne(id)
}

function saveContact(id, contact){
  store.saveOne(id, contact)
}

class ContactList extends Component {
  constructor(props){
    super(props)
    this.state = {
      contacts : [],
      detailsFor : -1,
      selected : null
    }
    this.selectContact = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_CONTACT_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('CONTACT_LOAD', () => {
      this.setState({
        contacts : store.content
      })
    })
  }
  render() {
    if (this.state.detailsFor === -1){
      return (
        <div id="background">
        <div>
            <ContactForm onAdd={addContact}/>
          </div>
          <div>
            <div id="listTitle"><h3>List of contacts</h3></div>
            <div id="formTitle"><h3>Add another contact</h3></div>
            
            <input type="button" class="Button" id="btnImport" value="Import contacts from GoogleContacts"/>
            {
              this.state.contacts.map((a) => 
                <Contact contact={a} key={a.id} onDelete={deleteContact} onSave={saveContact} onSelect={this.selectContact} />
              )
            }
            
          </div>
          
        </div>
      )
    }
    else{
      return (
        <div id="background">
          <ContactLabels onCancel={this.cancelSelection} contact={this.state.selected}/>
        </div>  
      )
    }
  }
}

export default ContactList
