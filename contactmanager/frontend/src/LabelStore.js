import axios from 'axios'

const SERVER = 'https://seminar22-costincanciu.c9users.io'

class LabelStore{
  constructor(ee){
    this.ee = ee
    this.content = []
  }
  getAll(contactId){
    axios(SERVER + '/contacts/' + contactId + '/labels')
      .then((response) => {
        this.content = response.data
        this.ee.emit('LABEL_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(contactId, label){
    axios.post(SERVER + '/contacts/' + contactId + '/labels', label)
      .then(() => this.getAll(contactId))
      .catch((error) => console.warn(error))
  }
  deleteOne(contactId, labelId){
    axios.delete(SERVER + '/contacts/' + contactId + '/labels/' + labelId) 
      .then(() => this.getAll(contactId))
      .catch((error) => console.warn(error))
  }
  saveOne(contactId, labelId, label){
    axios.put(SERVER + '/contacts/' + contactId + '/labels/' + labelId, label)
      .then(() => this.getAll(contactId))
      .catch((error) => console.warn(error))
  }
}

export default LabelStore




