import React, { Component } from 'react'
import LabelStore from './LabelStore'
import {EventEmitter} from 'fbemitter'
import Label from './Label'
import LabelForm from './LabelForm'

const ee = new EventEmitter()
const store = new LabelStore(ee)

class ContactLabels extends Component {
  constructor(props){
    super(props)
    this.state = {
      labels : []
    }
    this.addLabel = (label) => {
      store.addOne(this.props.contact.id, label)
    }
    this.saveLabel = (id, label) => {
      store.saveOne(this.props.contact.id, id, label)
    }
    this.deleteLabel = (id) => {
      store.deleteOne(this.props.contact.id, id)
    }
  }
  componentDidMount(){
    store.getAll(this.props.contact.id)
    ee.addListener('LABEL_LOAD', () => {
      this.setState({
        labels : store.content
      })
    })
  }
  render() {
    return (
      <div>

        <h3>List of labels for {this.props.contact.firstName+' '+this.props.contact.lastName}</h3>
        {
          this.state.labels.map((m) => 
            <Label label={m} onDelete={this.deleteLabel} onSave={this.saveLabel}/>
          )
        }
        <h3>Add another label </h3>
          <LabelForm onAdd={this.addLabel} />
        <input type="button" className="Button" value="Back" onClick={() => this.props.onCancel()} />
      </div>
    )
  }
}

export default ContactLabels
