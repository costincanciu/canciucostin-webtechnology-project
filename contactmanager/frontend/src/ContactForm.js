import React, { Component } from 'react'

class ContactForm extends Component {
  constructor(props){
    super(props)
    this.state = {
       firstName : '',
       lastName : '',
       email : '',
       phoneNumber : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
      console.warn(this.state)
    }
  }
  render() {
    return (
      <div className="Form">
        <div className="contactForm"><span className="contactField">First Name:  </span><input type="text" name="firstName" onChange={this.handleChange}/></div>
        <div className="contactForm"><span className="contactField">Last Name:   </span><input type="text" name="lastName" onChange={this.handleChange}/></div>
        <div className="contactForm"><span className="contactField">Phone Number:</span><input type="text" name="phoneNumber" onChange={this.handleChange}/></div>
        <div className="contactForm"><span className="contactField">Email:       </span><input type="text" name="email" onChange={this.handleChange}/></div>
        <div className="contactForm"><input className="Button" type="button" value="Add" onClick={() => this.props.onAdd({firstName : this.state.firstName,
        lastName : this.state.lastName,phoneNumber : this.state.phoneNumber, email : this.state.email})}/></div>
      </div>
    )
  }
}

export default ContactForm