import React, { Component } from 'react'

class Contact extends Component {
  constructor(props){
    super(props)
    this.state = {
      isEditing : false,
      firstName:'',
      lastName:'',
      email:'',
      phoneNumber:''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  componentDidMount(){
    this.setState({
      firstName:this.props.contact.firstName,
      lastName:this.props.contact.lastName,
      email:this.props.contact.email,
      phoneNumber:this.props.contact.phoneNumber,
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      isEditing : false
    })
  }
  render() {
    if (!this.state.isEditing){
      return (
        <div className="contactList">
                  <div className="contactForm"><img id="image" src="http://res.cloudinary.com/dhpqtjots/image/upload/v1516120698/placeholder-man_hr5ssi.png" alt="Smiley face" height="88" width="88"></img></div>

          <div className="contactForm"><span className="contactField">First Name:  {this.props.contact.firstName}</span></div>
          <div className="contactForm"><span className="contactField">Last  Name:  {this.props.contact.lastName}</span></div>
          <div className="contactForm"><span className="contactField">Email:       {this.props.contact.email}</span></div>
          
          <div className="contactForm"><span className="contactField">Phone Number:{this.props.contact.phoneNumber}</span></div>
          
          <div className="contactForm">
          <input type="button" className="Button" value="Delete" onClick={() => this.props.onDelete(this.props.contact.id)} />
          <input type="button" className="Button" value="Edit" onClick={() => this.setState({isEditing : true})}/>
          <input type="button" className="Button" value="Labels" onClick={() => this.props.onSelect(this.props.contact.id)}/></div>
        </div>
      )
    }
    else{
      return (
        <div>
          <input className="EditContacts" type="text" name="firstName" onChange={this.handleChange} value={this.state.firstName}/> 
          <input className="EditContacts" type="text" name="lastName" onChange={this.handleChange} value={this.state.lastName}/>
          <input className="EditContacts" type="text" name="phoneNumber" onChange={this.handleChange} value={this.state.phoneNumber}/>
          <input className="EditContacts" type="text" name="email" onChange={this.handleChange} value={this.state.email}/>
          <input className="EditContactsButton" type="button" value="Cancel" onClick={() => this.setState({isEditing : false})}/>
          <input className="EditContactsButton" type="button" value="Save" onClick={() => this.props.onSave(this.props.contact.id,
          {firstName : this.state.firstName, lastName:this.state.lastName,phoneNumber:this.state.phoneNumber, email : this.state.email})} />
        </div>  
      )
    }
  }
}

export default Contact