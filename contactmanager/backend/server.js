const express = require('express'),
      bodyParser = require('body-parser'),
      Sequelize = require('sequelize')



const sequelize=new Sequelize('rest_messages2','root','',{
    dialect:'mysql',
    define:{
        timestamps:false
    }
})

const User=sequelize.define('user',{
   
},{
    
})

const Contact=sequelize.define('contact',{
   
    firstName:{
        type:Sequelize.STRING,
        allownull:false,
        validate : { isAlpha:true }
    },
    lastName:{
        type:Sequelize.STRING,
        allownull:false,
        validate:{ isAlpha:true }
    },
    email:{
        type:Sequelize.STRING,
        allownull:false,
        validate: {isEmail:true }
    },
    phoneNumber:{
        type:Sequelize.STRING,
        allownull:false,
        validate: { isNumeric:true }
    },
    birthDate:{
      type:Sequelize.DATE,
      allownull:true,
      validate: {isDate:true,isAfter: "1900-01-01",isBefore:"2015-01-01"}
    }
});

const Label=sequelize.define('label',{
    name:{
        type:Sequelize.STRING,
        allownull:false,
    },
});

Contact.hasMany(Label)
User.hasMany(Contact)



const app=express()
app.use(bodyParser.json())
app.use(express.static('../frontend/build'))

app.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/users', (req, res, next) => {
  User.findAll()
    .then((users) => res.status(200).json(users))
    .catch((error) => next(error))
})

app.post('/users', (req, res, next) => {
  User.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/users/:id', (req, res, next) => {
  User.findById(req.params.id, {include : [Contact]})
    .then((user) => {
      if (user){
        res.status(200).json(user)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

app.put('/users/:id', (req, res, next) => {
  User.findById(req.params.id)
    .then((user) => {
      if (user){
        return user.update(req.body, {fields : ['id']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})




app.get('/contacts', (req, res, next) => {
  Contact.findAll()
    .then((contacts) => res.status(200).json(contacts))
    .catch((error) => next(error))
})

app.post('/contacts', (req, res, next) => {
  Contact.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/contacts/:id', (req, res, next) => {
  Contact.findById(req.params.id, {include : [Label]})
    .then((contact) => {
      if (contact){
        res.status(200).json(contact)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

app.put('/contacts/:id', (req, res, next) => {
  Contact.findById(req.params.id)
    .then((contact) => {
      if (contact){
        return contact.update(req.body, {fields : ['firstName', 'lastName','email','phoneNumber']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})


app.delete('/contacts/:id', (req, res, next) => {
  Contact.findById(req.params.id)
    .then((contact) => {
      if (contact){
        return contact.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.get('/contacts/:aid/labels', (req, res, next) => {
  Contact.findById(req.params.aid)
    .then((contact) => {
      if (contact){
        return contact.getLabels()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then((labels) => {
      if (!res.headersSent){
        res.status(200).json(labels)
      }
    })
    .catch((err) => next(err))
})

app.post('/contacts/:aid/labels', (req, res, next) => {
  Contact.findById(req.params.aid)
    .then((contact) => {
      if (contact){
        let label = req.body
        label.contact_id = contact.id
        return Label.create(label)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).json('created')
      }
    })
    .catch((err) => next(err))
})

app.get('/contacts/:aid/labels/:mid', (req, res, next) => {
  Label.findById(req.params.mid)
    .then((label) => {
      if (label){
        res.status(200).json(label)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((err) => next(err))
})

app.put('/contacts/:aid/labels/:mid', (req, res, next) => {
  Label.findById(req.params.mid)
    .then((label) => {
      if (label){
        return label.update(req.body, {fields : ['name']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))  
})

app.delete('/contacts/:aid/labels/:mid', (req, res, next) => {
  Label.findById(req.params.mid)
    .then((label) => {
      if (label){
        return label.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('removed')
      }
    })
    .catch((err) => next(err))
})
app.use((err, req, res, next) => {
  console.warn(err)
  res.status(500).send('some error...')
})

app.listen(8080)