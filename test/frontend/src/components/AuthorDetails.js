import React, { Component } from 'react'
import MessageStore from '../stores/MessageStore'
import {EventEmitter} from 'fbemitter'
import Message from './Message'
import MessageForm from './MessageForm'

const ee = new EventEmitter()
const store = new MessageStore(ee)

class AuthorDetails extends Component {
  constructor(props){
    super(props)
    this.state = {
      messages : []
    }
    this.addMessage = (message) => {
      store.addOne(this.props.author.id, message)
    }
    this.saveMessage = (id, message) => {
      store.saveOne(this.props.author.id, id, message)
    }
    this.deleteMessage = (id) => {
      store.deleteOne(this.props.author.id, id)
    }
  }
  componentDidMount(){
    store.getAll(this.props.author.id)
    ee.addListener('MESSAGE_LOAD', () => {
      this.setState({
        messages : store.content
      })
    })
  }
  render() {
    return (
      <div>
        <div>
          I am the details page for {this.props.author.name}. They can be contacted at {this.props.author.email}
        </div>
        <h3>List of messages</h3>
        {
          this.state.messages.map((m) => 
            <Message message={m} onDelete={this.deleteMessage} onSave={this.saveMessage}/>
          )
        }
        <h3>Add a new one </h3>
          <MessageForm onAdd={this.addMessage} />
        <input type="button" value="back" onClick={() => this.props.onCancel()} />
      </div>
    )
  }
}

export default AuthorDetails
