import React, { Component } from 'react'
import Author from './Author'
import AuthorForm from './AuthorForm'
import AuthorStore from '../stores/AuthorStore'
import {EventEmitter} from 'fbemitter'
import AuthorDetails from './AuthorDetails'

const ee = new EventEmitter()
const store = new AuthorStore(ee)

function addAuthor(author){
  store.addOne(author)
}

function deleteAuthor(id){
  store.deleteOne(id)
}

function saveAuthor(id, author){
  store.saveOne(id, author)
}

class AuthorList extends Component {
  constructor(props){
    super(props)
    this.state = {
      authors : [],
      detailsFor : -1,
      selected : null
    }
    this.selectAuthor = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_AUTHOR_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('AUTHOR_LOAD', () => {
      this.setState({
        authors : store.content
      })
    })
  }
  render() {
    if (this.state.detailsFor === -1){
      return (
        <div>
          <div>
            <h3>List of authors</h3>
            {
              this.state.authors.map((a) => 
                <Author author={a} key={a.id} onDelete={deleteAuthor} onSave={saveAuthor} onSelect={this.selectAuthor} />
              )
            }
          </div>
          <div>
            <AuthorForm onAdd={addAuthor}/>
          </div>
        </div>
      )
    }
    else{
      return (
        <div>
          <AuthorDetails onCancel={this.cancelSelection} author={this.state.selected}/>
        </div>  
      )
    }
  }
}

export default AuthorList
