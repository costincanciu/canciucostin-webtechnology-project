import React, {Component} from 'react'

class Message extends Component{
  constructor(props){
    super(props)
    this.state = {
      isEditing : false,
      messageTitle : '',
      messageContent : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  componentDidMount(){
    this.setState({
      messageTitle : this.props.message.title,
      messageContent : this.props.message.content
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      isEditing : false
    })
  }
  render(){
    if (!this.state.isEditing){
      return (
        <div>
          <h5>{this.props.message.title}</h5>
          {this.props.message.content}
          <div>
            <input type="button" value="delete" onClick={() => this.props.onDelete(this.props.message.id)} />
            <input type="button" value="edit" onClick={() => this.setState({isEditing : true})}/>
          </div>
        </div>  
      )
    }
    else{
      return (
        <div>
            <input type="text" name="messageTitle" onChange={this.handleChange} value={this.state.messageTitle}/> 
            <input type="text" name="messageContent" onChange={this.handleChange} value={this.state.messageContent}/>
            <input type="button" value="cancel" onClick={() => this.setState({isEditing : false})}/>
            <input type="button" value="save" onClick={() => this.props.onSave(this.props.message.id, {title : this.state.messageTitle, content : this.state.messageContent})} />
        </div>
      )
    }
  }
}

export default Message