import React, { Component } from 'react'

class MessageForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      messageTitle : '',
      messageContent : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render() {
    return (
      <div>
        Title : <input type="text" name="messageTitle" onChange={this.handleChange}/>
        Content : <input type="text" name="messageContent" onChange={this.handleChange}/>
        <input type="button" value="add" onClick={() => this.props.onAdd({title : this.state.messageTitle, content : this.state.messageContent})}/>
      </div>
    )
  }
}

export default MessageForm
